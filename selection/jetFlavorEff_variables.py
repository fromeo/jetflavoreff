#!/usr/bin/env python3
import os, sys
import ROOT
import math 
import numpy as np 

class jetFlavorEff_variables(object):
 def __init__(self, name):

  #Create file 
  inputFile = name
  outputFileName = os.path.basename(str(inputFile)).split(".root", 1)[0]+"_Skim.root"
  compression = "LZMA:9"
  ROOT.gInterpreter.ProcessLine("#include <Compression.h>")
  (algo, level) = compression.split(":")
  compressionLevel = int(level)
  if   algo == "LZMA": compressionAlgo  = ROOT.ROOT.kLZMA
  elif algo == "ZLIB": compressionAlgo  = ROOT.ROOT.kZLIB
  else: raise RuntimeError("Unsupported compression %s" % algo)
  self.outputfile = ROOT.TFile(outputFileName, 'RECREATE',"",compressionLevel)
  self.outputfile.SetCompressionAlgorithm(compressionAlgo)

  #Histograms
  #jet
  self.jetPt = ROOT.TH1F('jetPt','jetPt',100,20,2020)
  self.jetEta = ROOT.TH1F('jetEta','jetEta',48,-2.4,2.4)
  self.jetNvtx = ROOT.TH1F('jetNvtx','jetNvtx',15,0,60)
  #b quark
  #den 
  self.denBpartonFlavourPt = ROOT.TH1F('denBpartonFlavourPt','denBpartonFlavourPt',100,20,2020)
  self.denBpartonFlavourEta = ROOT.TH1F('denBpartonFlavourEta','denBpartonFlavourEta',48,-2.4,2.4)
  self.denBpartonFlavourNvtx = ROOT.TH1F('denBpartonFlavourNvtx','denBpartonFlavourNvtx',15,0,60)
  #num
  #deepJet=deepFlavour
  #pt 
  self.numBDeepFlavourLPt = ROOT.TH1F('numBDeepFlavourLPt','numBDeepFlavourLPt',100,20,2020)
  self.numBDeepFlavourMPt = ROOT.TH1F('numBDeepFlavourMPt','numBDeepFlavourMPt',100,20,2020)
  self.numBDeepFlavourTPt = ROOT.TH1F('numBDeepFlavourTPt','numBDeepFlavourTPt',100,20,2020)
  #eta 
  self.numBDeepFlavourLEta = ROOT.TH1F('numBDeepFlavourLEta','numBDeepFlavourLEta',48,-2.4,2.4)
  self.numBDeepFlavourMEta = ROOT.TH1F('numBDeepFlavourMEta','numBDeepFlavourMEta',48,-2.4,2.4)
  self.numBDeepFlavourTEta = ROOT.TH1F('numBDeepFlavourTEta','numBDeepFlavourTEta',48,-2.4,2.4)
  #nvtx
  self.numBDeepFlavourLNvtx = ROOT.TH1F('numBDeepFlavourLNvtx','numBDeepFlavourLNvtx',15,0,60)
  self.numBDeepFlavourMNvtx = ROOT.TH1F('numBDeepFlavourMNvtx','numBDeepFlavourMNvtx',15,0,60)
  self.numBDeepFlavourTNvtx = ROOT.TH1F('numBDeepFlavourTNvtx','numBDeepFlavourTNvtx',15,0,60)
  #deepCSV
  #pt 
  self.numBDeepCSVLPt = ROOT.TH1F('numBDeepCSVLPt','numBDeepCSVLPt',100,20,2020)
  self.numBDeepCSVMPt = ROOT.TH1F('numBDeepCSVMPt','numBDeepCSVMPt',100,20,2020)
  self.numBDeepCSVTPt = ROOT.TH1F('numBDeepCSVTPt','numBDeepCSVTPt',100,20,2020)
  #eta 
  self.numBDeepCSVLEta = ROOT.TH1F('numBDeepCSVLEta','numBDeepCSVLEta',48,-2.4,2.4)
  self.numBDeepCSVMEta = ROOT.TH1F('numBDeepCSVMEta','numBDeepCSVMEta',48,-2.4,2.4)
  self.numBDeepCSVTEta = ROOT.TH1F('numBDeepCSVTEta','numBDeepCSVTEta',48,-2.4,2.4)
  #nvtx
  self.numBDeepCSVLNvtx = ROOT.TH1F('numBDeepCSVLNvtx','numBDeepCSVLNvtx',15,0,60)
  self.numBDeepCSVMNvtx = ROOT.TH1F('numBDeepCSVMNvtx','numBDeepCSVMNvtx',15,0,60)
  self.numBDeepCSVTNvtx = ROOT.TH1F('numBDeepCSVTNvtx','numBDeepCSVTNvtx',15,0,60)

  #c quark
  #den 
  self.denCpartonFlavourPt = ROOT.TH1F('denCpartonFlavourPt','denCpartonFlavourPt',100,20,2020)
  self.denCpartonFlavourEta = ROOT.TH1F('denCpartonFlavourEta','denCpartonFlavourEta',48,-2.4,2.4)
  self.denCpartonFlavourNvtx = ROOT.TH1F('denCpartonFlavourNvtx','denCpartonFlavourNvtx',15,0,60)
  #num
  #deepJet=deepFlavour
  #pt 
  self.numCDeepFlavourLPt = ROOT.TH1F('numCDeepFlavourLPt','numCDeepFlavourLPt',100,20,2020)
  self.numCDeepFlavourMPt = ROOT.TH1F('numCDeepFlavourMPt','numCDeepFlavourMPt',100,20,2020)
  self.numCDeepFlavourTPt = ROOT.TH1F('numCDeepFlavourTPt','numCDeepFlavourTPt',100,20,2020)
  #eta 
  self.numCDeepFlavourLEta = ROOT.TH1F('numCDeepFlavourLEta','numCDeepFlavourLEta',48,-2.4,2.4)
  self.numCDeepFlavourMEta = ROOT.TH1F('numCDeepFlavourMEta','numCDeepFlavourMEta',48,-2.4,2.4)
  self.numCDeepFlavourTEta = ROOT.TH1F('numCDeepFlavourTEta','numCDeepFlavourTEta',48,-2.4,2.4)
  #nvtx
  self.numCDeepFlavourLNvtx = ROOT.TH1F('numCDeepFlavourLNvtx','numCDeepFlavourLNvtx',15,0,60)
  self.numCDeepFlavourMNvtx = ROOT.TH1F('numCDeepFlavourMNvtx','numCDeepFlavourMNvtx',15,0,60)
  self.numCDeepFlavourTNvtx = ROOT.TH1F('numCDeepFlavourTNvtx','numCDeepFlavourTNvtx',15,0,60)
  #deepCSV
  #pt 
  self.numCDeepCSVLPt = ROOT.TH1F('numCDeepCSVLPt','numCDeepCSVLPt',100,20,2020)
  self.numCDeepCSVMPt = ROOT.TH1F('numCDeepCSVMPt','numCDeepCSVMPt',100,20,2020)
  self.numCDeepCSVTPt = ROOT.TH1F('numCDeepCSVTPt','numCDeepCSVTPt',100,20,2020)
  #eta 
  self.numCDeepCSVLEta = ROOT.TH1F('numCDeepCSVLEta','numCDeepCSVLEta',48,-2.4,2.4)
  self.numCDeepCSVMEta = ROOT.TH1F('numCDeepCSVMEta','numCDeepCSVMEta',48,-2.4,2.4)
  self.numCDeepCSVTEta = ROOT.TH1F('numCDeepCSVTEta','numCDeepCSVTEta',48,-2.4,2.4)
  #nvtx
  self.numCDeepCSVLNvtx = ROOT.TH1F('numCDeepCSVLNvtx','numCDeepCSVLNvtx',15,0,60)
  self.numCDeepCSVMNvtx = ROOT.TH1F('numCDeepCSVMNvtx','numCDeepCSVMNvtx',15,0,60)
  self.numCDeepCSVTNvtx = ROOT.TH1F('numCDeepCSVTNvtx','numCDeepCSVTNvtx',15,0,60)

  #uds quark and g
  #den 
  self.denLGpartonFlavourPt = ROOT.TH1F('denLGpartonFlavourPt','denLGpartonFlavourPt',100,20,2020)
  self.denLGpartonFlavourEta = ROOT.TH1F('denLGpartonFlavourEta','denLGpartonFlavourEta',48,-2.4,2.4)
  self.denLGpartonFlavourNvtx = ROOT.TH1F('denLGpartonFlavourNvtx','denLGpartonFlavourNvtx',15,0,60)
  #num
  #deepJet=deepFlavour
  #pt 
  self.numLGDeepFlavourLPt = ROOT.TH1F('numLGDeepFlavourLPt','numLGDeepFlavourLPt',100,20,2020)
  self.numLGDeepFlavourMPt = ROOT.TH1F('numLGDeepFlavourMPt','numLGDeepFlavourMPt',100,20,2020)
  self.numLGDeepFlavourTPt = ROOT.TH1F('numLGDeepFlavourTPt','numLGDeepFlavourTPt',100,20,2020)
  #eta 
  self.numLGDeepFlavourLEta = ROOT.TH1F('numLGDeepFlavourLEta','numLGDeepFlavourLEta',48,-2.4,2.4)
  self.numLGDeepFlavourMEta = ROOT.TH1F('numLGDeepFlavourMEta','numLGDeepFlavourMEta',48,-2.4,2.4)
  self.numLGDeepFlavourTEta = ROOT.TH1F('numLGDeepFlavourTEta','numLGDeepFlavourTEta',48,-2.4,2.4)
  #nvtx
  self.numLGDeepFlavourLNvtx = ROOT.TH1F('numLGDeepFlavourLNvtx','numLGDeepFlavourLNvtx',15,0,60)
  self.numLGDeepFlavourMNvtx = ROOT.TH1F('numLGDeepFlavourMNvtx','numLGDeepFlavourMNvtx',15,0,60)
  self.numLGDeepFlavourTNvtx = ROOT.TH1F('numLGDeepFlavourTNvtx','numLGDeepFlavourTNvtx',15,0,60)
  #deepCSV
  #pt 
  self.numLGDeepCSVLPt = ROOT.TH1F('numLGDeepCSVLPt','numLGDeepCSVLPt',100,20,2020)
  self.numLGDeepCSVMPt = ROOT.TH1F('numLGDeepCSVMPt','numLGDeepCSVMPt',100,20,2020)
  self.numLGDeepCSVTPt = ROOT.TH1F('numLGDeepCSVTPt','numLGDeepCSVTPt',100,20,2020)
  #eta 
  self.numLGDeepCSVLEta = ROOT.TH1F('numLGDeepCSVLEta','numLGDeepCSVLEta',48,-2.4,2.4)
  self.numLGDeepCSVMEta = ROOT.TH1F('numLGDeepCSVMEta','numLGDeepCSVMEta',48,-2.4,2.4)
  self.numLGDeepCSVTEta = ROOT.TH1F('numLGDeepCSVTEta','numLGDeepCSVTEta',48,-2.4,2.4)
  #nvtx
  self.numLGDeepCSVLNvtx = ROOT.TH1F('numLGDeepCSVLNvtx','numLGDeepCSVLNvtx',15,0,60)
  self.numLGDeepCSVMNvtx = ROOT.TH1F('numLGDeepCSVMNvtx','numLGDeepCSVMNvtx',15,0,60)
  self.numLGDeepCSVTNvtx = ROOT.TH1F('numLGDeepCSVTNvtx','numLGDeepCSVTNvtx',15,0,60)

  #All entries 
  self.evtree = ROOT.TTree('evtree','evtree')
  self.add_float(self.evtree,"sumNumEvt")
  self.add_float(self.evtree,"sumgenWeight")

  #Efficiency/Num events
  self.effevt = ROOT.TTree('effevt','effevt')
  self.add_float(self.effevt,"passCut")
  self.add_float(self.effevt,"eegenWeight")
  self.add_float(self.effevt,"eelumiWeight")
        
  #Common variables 
  self.Events = ROOT.TTree('Events','Events')
  self.add_float(self.Events,"bJet_pt0")
  self.add_float(self.Events,"bJet_pt1")
  self.add_float(self.Events,"run")
  self.add_float(self.Events,"luminosityBlock")
  self.add_float(self.Events,"event")
  self.add_float(self.Events,"genWeight")
  self.add_float(self.Events,"lumiWeight")

 def add_float(self,tree,name,dtype=np.dtype(float)):
  if hasattr(self,name):
   print('ERROR! SetBranchAddress of name "%s" already exists!' % (name))
   exit(1)
  setattr(self,name,np.full((1),-99999999999999999999999999999999999999999999999999,dtype=dtype)) #1 elem w/ inizialization '-99999999999999999999999999999999999999999999999999'
  tree.Branch(name,getattr(self,name),'{0}/D'.format(name)) #The types in root (/D in this example) are defined here https://root.cern/root/html528/TTree.html

 def add_string(self,tree,name,dtype=np.dtype('S100')): #It assumes a string of max 100 characters
  if hasattr(self,name):
   print('ERROR! SetBranchAddress of name "%s" already exists!' % (name))
   exit(1)
  setattr(self,name,np.full((1),'noVal',dtype=dtype)) #1 elem w/ inizialization 'noval'
  tree.Branch(name,getattr(self,name),'{0}/C'.format(name))

 def add_vectorFloat(self,tree,name):
  if hasattr(self,name):
   print('ERROR! SetBranchAddress of name "%s" already exists!' % (name))
   exit(1)
  setattr(self,name,ROOT.std.vector('float')()) #No inizialization
  tree.Branch(name,getattr(self,name))

 def add_vectorString(self,tree,name):
  if hasattr(self,name):
   print('ERROR! SetBranchAddress of name "%s" already exists!' % (name))
   exit(1)
  setattr(self,name,ROOT.std.vector('string')()) #No inizialization
  tree.Branch(name,getattr(self,name))
