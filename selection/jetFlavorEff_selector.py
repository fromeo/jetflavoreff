#!/usr/bin/env python3
import os, sys
import ROOT
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module
from PhysicsTools.NanoAODTools.postprocessing.tools import matchObjectCollection, matchObjectCollectionMultiple

from jetFlavorEff_variables import *
from jetFlavorEff_functions import *

class declareVariables(jetFlavorEff_variables):
 def __init__(self, name):
  super(declareVariables, self).__init__(name)

class Producer(Module):
 def __init__(self, **kwargs):
  #User inputs
  self.channel     = kwargs.get('channel') 
  self.isData      = kwargs.get('dataType')=='data'
  self.year        = kwargs.get('year') 
  self.maxNumEvt   = kwargs.get('maxNumEvt')
  self.prescaleEvt = kwargs.get('prescaleEvt')
  self.lumiWeight  = kwargs.get('lumiWeight')

  #Analysis quantities
  if self.year==2018:
   #Trigger 
   print "not yet"
  elif self.year==2017:
   #Trigger
   if self.channel=="mumu":
    self.trigger = lambda e: e.HLT_IsoMu24

  elif self.year==2016:
   #Trigger
   if self.channel=="mumu":
    self.trigger = lambda e: e.HLT_IsoMu24

  else:
   raise ValueError('Year must be above 2016 (included).')

  #ID
  
  #Corrections

  #Cut flow table
        
 def beginJob(self):
  print "Here is beginJob"
  #pass
        
 def endJob(self):
  print "Here is endJob"
  #pass
        
 def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
  print "Here is beginFile"
  self.sumNumEvt = 0
  self.sumgenWeight = 0
  self.out = declareVariables(inputFile) 
  #pass
        
 def endFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):        
  print "Here is endFile"
  self.out.sumNumEvt[0] = self.sumNumEvt
  self.out.sumgenWeight[0] = self.sumgenWeight
  self.out.evtree.Fill()
  self.out.outputfile.Write()
  self.out.outputfile.Close()
  #pass
        
 def analyze(self, event):
  """process event, return True (go to next module) or False (fail, go to next event)"""
  #For all events
  if(self.sumNumEvt>self.maxNumEvt and self.maxNumEvt!=-1): return False
  self.sumNumEvt = self.sumNumEvt+1
  if not self.isData: self.sumgenWeight = self.sumgenWeight+(event.genWeight/abs(event.genWeight))
  if not self.sumNumEvt%self.prescaleEvt==0: return False
  passCut = 0 

  isLead = True
  isSubLead = True
  for ijet in range(event.nJet):
   if not event.Jet_pt[ijet]>=20: continue
   if not abs(event.Jet_eta[ijet])<=2.4: continue
   #jet
   self.out.jetPt.Fill(event.Jet_pt[ijet])
   self.out.jetEta.Fill(event.Jet_eta[ijet])
   self.out.jetNvtx.Fill(event.PV_npvsGood)
   #b quark
   if(abs(event.Jet_partonFlavour[ijet])==5):
    if(isSubLead and not isLead):
     self.out.bJet_pt1[0] = event.Jet_pt[ijet]
     isSubLead = False
    if(isLead):
     self.out.bJet_pt0[0] = event.Jet_pt[ijet]
     isLead = False
    #den 
    self.out.denBpartonFlavourPt.Fill(event.Jet_pt[ijet])
    self.out.denBpartonFlavourEta.Fill(event.Jet_eta[ijet])
    self.out.denBpartonFlavourNvtx.Fill(event.PV_npvsGood)
    #num
    #deepJet=deepFlavour
    #pt 
    if(event.Jet_btagDeepFlavB[ijet]>0.0490): self.out.numBDeepFlavourLPt.Fill(event.Jet_pt[ijet])
    if(event.Jet_btagDeepFlavB[ijet]>0.2783): self.out.numBDeepFlavourMPt.Fill(event.Jet_pt[ijet])
    if(event.Jet_btagDeepFlavB[ijet]>0.7100): self.out.numBDeepFlavourTPt.Fill(event.Jet_pt[ijet])
    #eta 
    if(event.Jet_btagDeepFlavB[ijet]>0.0490): self.out.numBDeepFlavourLEta.Fill(event.Jet_eta[ijet])
    if(event.Jet_btagDeepFlavB[ijet]>0.2783): self.out.numBDeepFlavourMEta.Fill(event.Jet_eta[ijet])
    if(event.Jet_btagDeepFlavB[ijet]>0.7100): self.out.numBDeepFlavourTEta.Fill(event.Jet_eta[ijet])
    #nvtx
    if(event.Jet_btagDeepFlavB[ijet]>0.0490): self.out.numBDeepFlavourLNvtx.Fill(event.PV_npvsGood)
    if(event.Jet_btagDeepFlavB[ijet]>0.2783): self.out.numBDeepFlavourMNvtx.Fill(event.PV_npvsGood)
    if(event.Jet_btagDeepFlavB[ijet]>0.7100): self.out.numBDeepFlavourTNvtx.Fill(event.PV_npvsGood)
    #deepCSV
    #pt 
    if(event.Jet_btagDeepB[ijet]>0.1208): self.out.numBDeepCSVLPt.Fill(event.Jet_pt[ijet])
    if(event.Jet_btagDeepB[ijet]>0.4168): self.out.numBDeepCSVMPt.Fill(event.Jet_pt[ijet])
    if(event.Jet_btagDeepB[ijet]>0.7665): self.out.numBDeepCSVTPt.Fill(event.Jet_pt[ijet])
    #eta 
    if(event.Jet_btagDeepB[ijet]>0.1208): self.out.numBDeepCSVLEta.Fill(event.Jet_eta[ijet])
    if(event.Jet_btagDeepB[ijet]>0.4168): self.out.numBDeepCSVMEta.Fill(event.Jet_eta[ijet])
    if(event.Jet_btagDeepB[ijet]>0.7665): self.out.numBDeepCSVTEta.Fill(event.Jet_eta[ijet])
    #nvtx
    if(event.Jet_btagDeepB[ijet]>0.1208): self.out.numBDeepCSVLNvtx.Fill(event.PV_npvsGood)
    if(event.Jet_btagDeepB[ijet]>0.4168): self.out.numBDeepCSVMNvtx.Fill(event.PV_npvsGood)
    if(event.Jet_btagDeepB[ijet]>0.7665): self.out.numBDeepCSVTNvtx.Fill(event.PV_npvsGood)

   #c quark
   if(abs(event.Jet_partonFlavour[ijet])==4):
    #den 
    self.out.denCpartonFlavourPt.Fill(event.Jet_pt[ijet])
    self.out.denCpartonFlavourEta.Fill(event.Jet_eta[ijet])
    self.out.denCpartonFlavourNvtx.Fill(event.PV_npvsGood)
    #num
    #deepJet=deepFlavour
    #pt 
    if(event.Jet_btagDeepFlavB[ijet]>0.0490): self.out.numCDeepFlavourLPt.Fill(event.Jet_pt[ijet])
    if(event.Jet_btagDeepFlavB[ijet]>0.2783): self.out.numCDeepFlavourMPt.Fill(event.Jet_pt[ijet])
    if(event.Jet_btagDeepFlavB[ijet]>0.7100): self.out.numCDeepFlavourTPt.Fill(event.Jet_pt[ijet])
    #eta 
    if(event.Jet_btagDeepFlavB[ijet]>0.0490): self.out.numCDeepFlavourLEta.Fill(event.Jet_eta[ijet])
    if(event.Jet_btagDeepFlavB[ijet]>0.2783): self.out.numCDeepFlavourMEta.Fill(event.Jet_eta[ijet])
    if(event.Jet_btagDeepFlavB[ijet]>0.7100): self.out.numCDeepFlavourTEta.Fill(event.Jet_eta[ijet])
    #nvtx
    if(event.Jet_btagDeepFlavB[ijet]>0.0490): self.out.numCDeepFlavourLNvtx.Fill(event.PV_npvsGood)
    if(event.Jet_btagDeepFlavB[ijet]>0.2783): self.out.numCDeepFlavourMNvtx.Fill(event.PV_npvsGood)
    if(event.Jet_btagDeepFlavB[ijet]>0.7100): self.out.numCDeepFlavourTNvtx.Fill(event.PV_npvsGood)
    #deepCSV
    #pt 
    if(event.Jet_btagDeepB[ijet]>0.1208): self.out.numCDeepCSVLPt.Fill(event.Jet_pt[ijet])
    if(event.Jet_btagDeepB[ijet]>0.4168): self.out.numCDeepCSVMPt.Fill(event.Jet_pt[ijet])
    if(event.Jet_btagDeepB[ijet]>0.7665): self.out.numCDeepCSVTPt.Fill(event.Jet_pt[ijet])
    #eta 
    if(event.Jet_btagDeepB[ijet]>0.1208): self.out.numCDeepCSVLEta.Fill(event.Jet_eta[ijet])
    if(event.Jet_btagDeepB[ijet]>0.4168): self.out.numCDeepCSVMEta.Fill(event.Jet_eta[ijet])
    if(event.Jet_btagDeepB[ijet]>0.7665): self.out.numCDeepCSVTEta.Fill(event.Jet_eta[ijet])
    #nvtx
    if(event.Jet_btagDeepB[ijet]>0.1208): self.out.numCDeepCSVLNvtx.Fill(event.PV_npvsGood)
    if(event.Jet_btagDeepB[ijet]>0.4168): self.out.numCDeepCSVMNvtx.Fill(event.PV_npvsGood)
    if(event.Jet_btagDeepB[ijet]>0.7665): self.out.numCDeepCSVTNvtx.Fill(event.PV_npvsGood)

   #uds quark or g
   if((1<=abs(event.Jet_partonFlavour[ijet]) and abs(event.Jet_partonFlavour[ijet])<=3) or abs(event.Jet_partonFlavour[ijet])==21):
    #den 
    self.out.denLGpartonFlavourPt.Fill(event.Jet_pt[ijet])
    self.out.denLGpartonFlavourEta.Fill(event.Jet_eta[ijet])
    self.out.denLGpartonFlavourNvtx.Fill(event.PV_npvsGood)
    #num
    #deepJet=deepFlavour
    #pt 
    if(event.Jet_btagDeepFlavB[ijet]>0.0490): self.out.numLGDeepFlavourLPt.Fill(event.Jet_pt[ijet])
    if(event.Jet_btagDeepFlavB[ijet]>0.2783): self.out.numLGDeepFlavourMPt.Fill(event.Jet_pt[ijet])
    if(event.Jet_btagDeepFlavB[ijet]>0.7100): self.out.numLGDeepFlavourTPt.Fill(event.Jet_pt[ijet])
    #eta 
    if(event.Jet_btagDeepFlavB[ijet]>0.0490): self.out.numLGDeepFlavourLEta.Fill(event.Jet_eta[ijet])
    if(event.Jet_btagDeepFlavB[ijet]>0.2783): self.out.numLGDeepFlavourMEta.Fill(event.Jet_eta[ijet])
    if(event.Jet_btagDeepFlavB[ijet]>0.7100): self.out.numLGDeepFlavourTEta.Fill(event.Jet_eta[ijet])
    #nvtx
    if(event.Jet_btagDeepFlavB[ijet]>0.0490): self.out.numLGDeepFlavourLNvtx.Fill(event.PV_npvsGood)
    if(event.Jet_btagDeepFlavB[ijet]>0.2783): self.out.numLGDeepFlavourMNvtx.Fill(event.PV_npvsGood)
    if(event.Jet_btagDeepFlavB[ijet]>0.7100): self.out.numLGDeepFlavourTNvtx.Fill(event.PV_npvsGood)
    #deepCSV
    #pt 
    if(event.Jet_btagDeepB[ijet]>0.1208): self.out.numLGDeepCSVLPt.Fill(event.Jet_pt[ijet])
    if(event.Jet_btagDeepB[ijet]>0.4168): self.out.numLGDeepCSVMPt.Fill(event.Jet_pt[ijet])
    if(event.Jet_btagDeepB[ijet]>0.7665): self.out.numLGDeepCSVTPt.Fill(event.Jet_pt[ijet])
    #eta 
    if(event.Jet_btagDeepB[ijet]>0.1208): self.out.numLGDeepCSVLEta.Fill(event.Jet_eta[ijet])
    if(event.Jet_btagDeepB[ijet]>0.4168): self.out.numLGDeepCSVMEta.Fill(event.Jet_eta[ijet])
    if(event.Jet_btagDeepB[ijet]>0.7665): self.out.numLGDeepCSVTEta.Fill(event.Jet_eta[ijet])
    #nvtx
    if(event.Jet_btagDeepB[ijet]>0.1208): self.out.numLGDeepCSVLNvtx.Fill(event.PV_npvsGood)
    if(event.Jet_btagDeepB[ijet]>0.4168): self.out.numLGDeepCSVMNvtx.Fill(event.PV_npvsGood)
    if(event.Jet_btagDeepB[ijet]>0.7665): self.out.numLGDeepCSVTNvtx.Fill(event.PV_npvsGood)

  #Event
  self.out.run[0] = event.run
  self.out.luminosityBlock[0] = event.luminosityBlock
  self.out.event[0] = event.event #& 0xffffffffffffffff

  #Save tree
  self.out.Events.Fill() 
  return True
