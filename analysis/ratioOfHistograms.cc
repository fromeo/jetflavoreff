/**
This Macro   
1. 

Need to specify
1. See Declare constants

Notes
*/
/////
//   To run: root -l ratioOfHistograms.cc+ 
/////
/////
//   Prepare Root and Roofit
/////
#include "TFile.h"
#include "TH1F.h"
#include "TTree.h"
#include "TTreePlayer.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TGraphAsymmErrors.h"
#include "TEfficiency.h"
#include <fstream>
#include <iostream>
#include <iomanip>
#include <iostream>
/////
//   Declare constants
/////
const string path = "/eos/cms/store/group/phys_exotica/LQtop/jetFlavorEff/2017/mergedFiles/";
const string rootpla = "all.root"; //ttHJetTobb_M125.root ZprimeToBB_narrow_M-2000.root ZprimeToBB_narrow_M-4000.root
const string denCase = "denLGpartonFlavour"; //denBpartonFlavour denCpartonFlavour denLGpartonFlavour
const string numCase = "numLGDeepFlavour"; //numBDeepFlavour numBDeepCSV numCDeepFlavour numCDeepCSV numLGDeepFlavour numLGDeepCSV
const int numWorkingPoints = 3;
const string workingPoints[numWorkingPoints] = {"L", "M", "T"};
//Choose the variable on X:
const bool pt_plots = true;
const string titleXaxis = "pT (GeV)"; const int lmcol = 4; //Blue
const string part = "Pt";
const bool eta_plots = false;
/*
const string titleXaxis = "#eta"; const int lmcol = 8; //Blue
const string part = "Eta";
*/
const bool nVert_plots = false;
/*
const string titleXaxis = "# Vert"; const int lmcol = 6; //Blue
const string part = "Nvtx";
*/

TFile* Call_TFile();
void hist_ratio(TH1* hpn, TH1* hpd, string workingPoint, string varname);
void setTDRStyle();
/////
//   Main function
/////
void ratioOfHistograms() {
 setTDRStyle();
 //Call tree 
 TFile* f = Call_TFile();

 string denCase_ = denCase+part;
 TH1F* den; f->GetObject(denCase_.c_str(),den);
 for(int nwp=0; nwp<numWorkingPoints; nwp++){
  string numCase_ = numCase+workingPoints[nwp]+part;
  TH1F* num; f->GetObject(numCase_.c_str(),num);
  string nameFile = numCase_+"_"+denCase_;
  hist_ratio(num,den,workingPoints[nwp],nameFile);
 }
}
/////
//   Call TFile to be read
/////
TFile* Call_TFile() {
 string file_name = path+rootpla;
 TFile* f = new TFile(file_name.c_str(),"update");
 return f;
}
/////
//   Initialize the strings 
/////
void hist_ratio(TH1* hpn, TH1* hpd, string workingPoint, string varname){
 TCanvas* c1 = new TCanvas(varname.c_str(),varname.c_str(),100,100,700,500);
 c1->SetLogy();
 TGraphAsymmErrors* hpratio = new TGraphAsymmErrors(hpn,hpd);
 hpratio->BayesDivide(hpn,hpd);
 hpratio->SetLineColor(lmcol);
 hpratio->SetMarkerColor(lmcol);
 hpratio->SetTitle();
 hpratio->GetXaxis()->SetTitle(titleXaxis.c_str());
 string titleYaxis;
 titleYaxis = "#epsilon ("+workingPoint+")";//"+(string) bin_size_c;
 hpratio->GetYaxis()->SetTitle(titleYaxis.c_str());
 hpratio->GetYaxis()->SetRangeUser(0,1.25);
 hpratio->Draw("PAZ9");
 c1->Update();
 string ext = ".png";
 string namefile = varname+ext;
 c1->SaveAs(namefile.c_str());
}
/////
//   Set setTDRStyle_modified (from link https://twiki.cern.ch/twiki/pub/CMS/TRK10001/setTDRStyle_modified.C)
/////
void setTDRStyle(){
  TStyle *tdrStyle = new TStyle("tdrStyle","Style for P-TDR");
  //For the canvas:
  tdrStyle->SetCanvasBorderMode(0);
  tdrStyle->SetCanvasColor(kWhite);
  tdrStyle->SetCanvasDefH(600); //Height of canvas
  tdrStyle->SetCanvasDefW(600); //Width of canvas
  tdrStyle->SetCanvasDefX(0);   //POsition on screen
  tdrStyle->SetCanvasDefY(0);
  //For the Pad:
  tdrStyle->SetPadBorderMode(0);
  //tdrStyle->SetPadBorderSize(Width_t size = 1);
  tdrStyle->SetPadColor(kWhite);
  tdrStyle->SetPadGridX(false);
  tdrStyle->SetPadGridY(false);
  tdrStyle->SetGridColor(0);
  tdrStyle->SetGridStyle(3);
  tdrStyle->SetGridWidth(1);
  // For the frame:
  tdrStyle->SetFrameBorderMode(0);
  tdrStyle->SetFrameBorderSize(1);
  tdrStyle->SetFrameFillColor(0);
  tdrStyle->SetFrameFillStyle(0);
  tdrStyle->SetFrameLineColor(1);
  tdrStyle->SetFrameLineStyle(1);
  tdrStyle->SetFrameLineWidth(1);
  //For the histo:
  tdrStyle->SetHistFillColor(0);
  //tdrStyle->SetHistFillStyle(0);
  tdrStyle->SetHistLineColor(1);
  tdrStyle->SetHistLineStyle(0);
  tdrStyle->SetHistLineWidth(1);
  //tdrStyle->SetLegoInnerR(Float_t rad = 0.5);
  //tdrStyle->SetNumberContours(Int_t number = 20);
  //tdrStyle->SetEndErrorSize(0);
  tdrStyle->SetErrorX(0.);
  //tdrStyle->SetErrorMarker(20);
  tdrStyle->SetMarkerStyle(20);
  //For the fit/function:
  tdrStyle->SetOptFit(1111);
  tdrStyle->SetFitFormat("5.4g");
  //tdrStyle->SetFuncColor(1);
  tdrStyle->SetFuncStyle(1);
  tdrStyle->SetFuncWidth(1);
  //For the date:
  tdrStyle->SetOptDate(0);
  //tdrStyle->SetDateX(Float_t x = 0.01);
  //tdrStyle->SetDateY(Float_t y = 0.01);
  //For the statistics box:
  tdrStyle->SetOptFile(0);
  tdrStyle->SetOptStat(1111); // To display the mean and RMS:   SetOptStat("mr");
  tdrStyle->SetStatColor(kWhite);
  tdrStyle->SetStatFont(42);
  tdrStyle->SetStatFontSize(0.025);
  tdrStyle->SetStatTextColor(1);
  tdrStyle->SetStatFormat("6.4g");
  tdrStyle->SetStatBorderSize(1);
  tdrStyle->SetStatH(0.1);
  tdrStyle->SetStatW(0.15);
  //tdrStyle->SetStatStyle(Style_t style = 1001);
  //tdrStyle->SetStatX(Float_t x = 0);
  //tdrStyle->SetStatY(Float_t y = 0);
  //Margins:
  tdrStyle->SetPadTopMargin(0.05);
  tdrStyle->SetPadBottomMargin(0.13);
  tdrStyle->SetPadLeftMargin(0.13);
  tdrStyle->SetPadRightMargin(0.05);
  //For the Global title:
  //tdrStyle->SetOptTitle(0);
  tdrStyle->SetTitleX(0.1f);
  tdrStyle->SetTitleW(0.8f);
  tdrStyle->SetTitleFont(42);
  tdrStyle->SetTitleColor(1);
  tdrStyle->SetTitleTextColor(1);
  tdrStyle->SetTitleFillColor(10);
  tdrStyle->SetTitleFontSize(0.05);
  //tdrStyle->SetTitleH(0); // Set the height of the title box
  //tdrStyle->SetTitleW(0); // Set the width of the title box
  //tdrStyle->SetTitleX(0); // Set the position of the title box
  //tdrStyle->SetTitleY(0.985); // Set the position of the title box
  //tdrStyle->SetTitleStyle(Style_t style = 1001);
  //tdrStyle->SetTitleBorderSize(2);
  //For the axis titles:
  tdrStyle->SetTitleColor(1, "XYZ");
  tdrStyle->SetTitleFont(42, "XYZ");
  tdrStyle->SetTitleSize(0.06, "X");
  tdrStyle->SetTitleSize(0.075, "Y");
  //tdrStyle->SetTitleXSize(Float_t size = 0.02); // Another way to set the size?
  //tdrStyle->SetTitleYSize(Float_t size = 0.02);
  tdrStyle->SetTitleXOffset(0.9);
  tdrStyle->SetTitleYOffset(0.7);
  //tdrStyle->SetTitleOffset(1.1, "Y"); // Another way to set the Offset
  //For the axis labels:
  tdrStyle->SetLabelColor(1, "XYZ");
  tdrStyle->SetLabelFont(42, "XYZ");
  tdrStyle->SetLabelOffset(0.007, "XYZ");
  tdrStyle->SetLabelSize(0.05, "XYZ");
  //For the axis:
  tdrStyle->SetAxisColor(1, "XYZ");
  tdrStyle->SetStripDecimals(kTRUE);
  tdrStyle->SetTickLength(0.03, "XYZ");
  tdrStyle->SetNdivisions(510, "XYZ");
  tdrStyle->SetPadTickX(1);  // To get tick marks on the opposite side of the frame
  tdrStyle->SetPadTickY(1);
  //Change for log plots:
  tdrStyle->SetOptLogx(0);
  tdrStyle->SetOptLogy(0);
  tdrStyle->SetOptLogz(0);
  // Postscript options:
  // tdrStyle->SetPaperSize(15.,15.);
  // tdrStyle->SetLineScalePS(Float_t scale = 3);
  // tdrStyle->SetLineStyleString(Int_t i, const char* text);
  // tdrStyle->SetHeaderPS(const char* header);
  // tdrStyle->SetTitlePS(const char* pstitle);
  // tdrStyle->SetBarOffset(Float_t baroff = 0.5);
  // tdrStyle->SetBarWidth(Float_t barwidth = 0.5);
  // tdrStyle->SetPaintTextFormat(const char* format = "g");
  // tdrStyle->SetPalette(Int_t ncolors = 0, Int_t* colors = 0);
  // tdrStyle->SetTimeOffset(Double_t toffset);
  // tdrStyle->SetHistMinimumZero(kTRUE);
  tdrStyle->cd();
}

double deltaPhi(double phi1, double phi2) {
 double result = phi1 - phi2;
 while (result > M_PI) result -= 2*M_PI;
 while (result <= -M_PI) result += 2*M_PI;
 return result;
}

